# Security Advisories

In this repository, you can find some security advisories I published.

### List of advisories
[XSS vulnerablity in tooltip plugin of Zurb Foundation 5](https://bitbucket.org/wneessen/advisories/src/b9e62497e993359c32a3c9bcd3de560bccb6aeaf/Foundation5/XSS%20in%20Tooltips%20plugin%20of%20Foundation%205.x/ "XSS vulnerablity in tooltip plugin of Zurb Foundation 5")
